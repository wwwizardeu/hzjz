let HOME = '/'; // change this for subfolder install
let dataTable;

if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}

document.addEventListener("DOMContentLoaded", function() {

    console.log('JavaScript: Loaded');

    $(document).ready( function () {
        dataTable = $('.js-entries').DataTable({
            order: [],
            language: {
                "emptyTable": "Nema podataka u tablici",
                "info": "Prikazano _START_ do _END_ od _TOTAL_ nalaza",
                "infoEmpty": "Prikazano 0 do 0 od 0 nalaza",
                "infoFiltered": "(filtrirano iz _MAX_ ukupnih nalaza)",
                "infoThousands": ",",
                "lengthMenu": "_MENU_ nalaza po stranici",
                "loadingRecords": "Dohvaćam...",
                "processing": "Obrađujem...",
                "search": "",
                "zeroRecords": "Ništa nije pronađeno",
                "paginate": {
                    "first": "Prva",
                    "previous": "Nazad",
                    "next": "Naprijed",
                    "last": "Zadnja"
                },
                "autoFill": {
                    "cancel": "Poništi"
                },
            }
        });
    } );

});

(function ($) {

    // Skroliranje stranice da je element vidljiv
    $.fn.scrollIntoView = function (duration) {
        if (this.length) {
            var top = Math.max(window.scrollY + $(window).height(), this.offset().top + this.height() + 75) - $(window).height();
            top = Math.min(top, this.offset().top - 75);
            $('html, body').animate({scrollTop: top}, duration || 'slow');
        }
        return this;
    };

    // ================= HTTP requesti =================

    // Low-level HTTP request
    function request(method, url, data, callback, progress) {
        if (data instanceof jQuery)
            data = (data.is(':input') ? data : $(':input', data)).serialize();
        if (data instanceof Object && !('FormData' in window && data instanceof FormData))
            data = $.param(data);

        var xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.timeout = 10000;
        xhr.onload = handler;
        xhr.onerror = handler;
        xhr.ontimeout = handler;
        if (xhr.upload)
            xhr.upload.onprogress = function (e) {
                xhr.timeout = 0;
                if (e.lengthComputable && typeof progress == 'function')
                    progress.call(xhr, Math.floor(e.loaded / e.total * 100));
            };
        if (typeof data == 'string')
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send(data);

        function handler(e) {
            try {
                var data = (this.responseType == 'json' ? this.response : JSON.parse(this.responseText));
            } catch (e) {}
            if (typeof callback == 'function')
                callback.call(this, this.status, data)
        }
    }

    // HTTP request s disable-anjem gumba
    $.fn.request = function (method, url, data, success, error, progress) {
        if (data instanceof jQuery)
            data = (data.is(':input') ? data : $(':input', data)).serialize();

        var $button = this.not('[disabled]')
            .prop('disabled', true);

        request(method, url, data, callback, progress);

        function callback(status, response) {
            $button.prop('disabled', false);

            var errormessage;
            if (response && typeof response == "object" && response.error)
                errormessage = response.error;
            else if (!status)
                errormessage = 'No response from the server.';
            else if (status < 200 || status >= 400)
                errormessage = status+' '+this.statusText;
            else if (!response || typeof response != "object")
                errormessage = "Invalid server response.";
            else if (typeof success == "function")
                success.call($button[0], response);
            if (errormessage && typeof error == "function")
                errormessage = error.call($button[0], errormessage);
            if (errormessage)
                modalAlert('Neočekivana greška', errormessage);
        }

        return this;
    };

    // HTTP request sa slanjem čitavog formulara i prikazom greške u njemu
    $.fn.formRequest = function (method, url, data, success, error, progress) {
        var $form = (this.is('form') ? this : $('form', this));
        var $button = $('button, [type="submit"]', this), $disabled = $([]);

        $('[type="file"]', $form).each(function () {
            if (!this.files.length)
                $disabled = $disabled.add($(this).prop('disabled', true));
        });
        var formdata = ($form.length ? new FormData($form[0]) : new FormData());
        $disabled.prop('disabled', false);
        if (!$form.length)
            $.each($(':input', this).serializeArray(), function () {
                formdata.append(this.name, this.value);
            });
        if (data instanceof jQuery)
            $.each((data.is(':input, form') ? data : $(':input', data)).serializeArray(), function () {
                formdata.append(this.name, this.value);
            });
        else if (typeof data == 'object')
            $.each(data, function (key, value) {
                formdata.append(key, value);
            });

        var $error = $('.js-error', $form).slideUp();

        $button.request(method, url, formdata, success,
            function (errorMessage) {
                if (typeof error == 'function')
                    errorMessage = error.call($button[0], errorMessage);
                if (typeof errorMessage == 'string')
                    if ($error.length)
                        $error.text(errorMessage).slideDown(200).scrollIntoView();
                    else
                        modalAlert('Neočekivana greška', errorMessage);
            },
            progress
        );
        return this;
    };

    // ================= Modali (improvizirani) =================

    // Generički modal za "are you sure?"
    function modalConfirm(title, body, button, callback) {
        if (confirm(title.toUpperCase() + "\n\n" + body))
            callback();
    }

    // Generički modal za kratke obavijesti
    function modalAlert(title, body, button) {
        alert(body);
    }

    // ================= Formulari =================

    var $loginForm = $('.js-login-form')
        .on('submit', function (e) {
            e.preventDefault();
            $('.c-form__error', $loginForm).hide();
            $loginForm.formRequest('POST', HOME+'login', {},
                function () {
                    window.location.reload();
                },
                function (error) {
                    $('.c-form__error', $loginForm).fadeIn().slideDown().scrollIntoView()
                        .find('.js-error-message').text(error);
                },
            );
        });

    var $codeForm = $('.js-code-form')
        .on('submit', function (e) {
            e.preventDefault();
            $('.c-form__error', $codeForm).hide();
            $codeForm.formRequest('POST', this.action, {},
                function (r) {
                    $('.c-front__form').hide();
                    var $body = $('.c-front__body--static, .c-front__body--'+r.result)
                        .fadeIn().scrollIntoView();
                    $('.js-document', $body).toggle(!!r.url)
                        .find('a').attr('href', r.url);
                    $('.js-comment', $body).toggle(!!r.comment)
                        .find('span').text(r.comment);
                },
                function (error) {
                    $('.c-form__error', $codeForm).fadeIn().slideDown().scrollIntoView()
                        .find('.js-error-message').text(error);
                },
            )
        });

    var $entryForm = $('.js-entry-form')
        .on('submit', function (e) {
            e.preventDefault();
            $(this).formRequest('POST', this.action, {},
                function (r) {
                    var isRepeat = (e.originalEvent.submitter && e.originalEvent.submitter.value == 'repeat');
                    if (isRepeat) {
                        $('.js-notice').fadeIn().scrollIntoView()
                            .find('span').text(`Nalaz »${r.code}« je uspješno unesen!`);
                        $('[type="text"], [type="file"], textarea', $entryForm).val('')
                            .first().focus();
                        $('[type="radio"]', $entryForm).prop('checked', false);
                        $entryForm[0].reset();
                    } else
                        window.location = HOME+'admin';
                },
                function (error) {
                    $('.js-notice').fadeIn().scrollIntoView()
                        .find('span').text(error);
                });
        });

    $('.js-delete-entry').on('click', function () {
        var $row = $(this).closest('[data-code]'), code = $row.attr('data-code');
        if (confirm(`Obrisati nalaz sa šifrom "${code}"?`))
            $(this).request('DELETE', HOME+'admin/nalaz/'+encodeURIComponent(code), {}, function () {
                dataTable.row($row).remove().draw();
            });
    });

    var delete_cookie = function(name) {
        document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    };

    $('.js-logout').on('click', function(e) {
        e.preventDefault();
        delete_cookie('user');
        window.location.reload();
    });

})(jQuery);
