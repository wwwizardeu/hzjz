import { gulp, src, dest, watch, series, parallel } from 'gulp';

import sass from 'gulp-sass';
import gulpif from 'gulp-if';
import postcss from 'gulp-postcss';
import cleanCss from 'gulp-clean-css';
import sourcemaps from 'gulp-sourcemaps';
import autoprefixer from 'autoprefixer';
//import styleLinter from 'gulp-stylelint';

import webpack from 'webpack-stream';

import svgSprite  from 'gulp-svg-sprite';
import svgmin from 'gulp-svgmin';
    
import browserSync from 'browser-sync';

import yargs from 'yargs';
import babel from 'gulp-babel';

import zip from 'gulp-zip';
import rsync from 'gulp-rsync';

/**
* Configuration
* 
*/

const PRODUCTION = yargs.argv.production;
const bs = browserSync.create();

const config = {
    domain: 'hzjz' + '.test',
    src: './source',
    dist: './public/assets',
    server: {
        host: '',
        username: PRODUCTION ? '' : '',
        port: 21,
        webroot: PRODUCTION ? '' : ''
    }
}

/**
* Browsersync
* 
*/

export const server = done => {
    bs.init({
        proxy:  'https://' + config.domain,
        host:   config.domain,
        open:   'external',
        baseDir: "./public"
    });
    
    done();
}

export const reload = done => {
    bs.reload();
    done();
};


/**
* (S)CSS
* 
*/

export const styles = () => {
    return src([config.src + '/scss/app.scss'])
        .pipe(gulpif(!PRODUCTION, sourcemaps.init()))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulpif(PRODUCTION, postcss([ autoprefixer ])))
        .pipe(gulpif(PRODUCTION, cleanCss()))
        .pipe(gulpif(!PRODUCTION, sourcemaps.write('.')))
        .pipe(dest(config.dist + '/css'))
        .pipe(bs.stream());
}

export const stylelint = () => {
    return src([config.src + '/scss/**/*.scss'])
        .pipe(styleLinter({
            failAfterError: false,
            reporters: [{formatter: 'string', console: true}]
      }));
}

export const stylelintfix = () => {
    return src([config.src + '/scss/**/*.scss'])
        .pipe(styleLinter({
            fix: true,
            reporters: [{formatter: 'string', console: true}]
        }))
        .pipe(dest(config.src + '/scss'));
}

/**
* JavaScript
* 
*/

export const scripts = () => {
    return src(config.src + '/js/app.js')
    .pipe(webpack({
      module: {
        rules: [
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: [
                    ['@babel/preset-env', { targets: "ie 11" }]
                ]
              }
            }
          }
        ]
      },
      mode: PRODUCTION ? 'production' : 'development',
      devtool: !PRODUCTION ? 'inline-source-map' : false,
      output: {
        filename: 'app.js'
      },
      externals: {
        jquery: 'jQuery'
      },
    }))
    .pipe(dest(config.dist + '/js'));
}

/**
* Sprites
* 
*/

export const sprite = () => {
    return(src(config.src + '/sprite/**/*.svg'))
        .pipe(svgmin({
            plugins: [
                { removeAttrs: { attrs: '.*:(fill|stroke):.*' } }, 
                { removeUselessStrokeAndFill: true },
                { inlineStyles: false },
                { removeDimensions: true }
            ]}))
        .pipe(svgSprite({
            mode: {
                symbol: {
                    dest : ".",
                    sprite : "sprite.svg",
                    example: {
                        dest: 'sprite.html'
                    }
                }
            }
        }))
        .pipe(dest(config.dist + '/sprite'));
}

/**
* Watch function
* 
*/

export const watchForChanges = () => {
    watch(config.src + '/scss/**/*.scss', series(styles, reload));
    watch(config.src + '/js/**/*.js', series(scripts, reload));
    watch(config.src + '/sprite/**/*.svg', series(sprite, reload));
    watch(config.dist + '/**/*.php', reload);
    watch(config.dist + '/**/*.html', reload);
}

/**
* Deploy function
* 
*/


export const deploy = () => {
    return src(['public/**', '!public/app/{cache,cache/**}', '!public/{upload,upload/**}', '!public/{cutup,cutup/**}'])
        .pipe(rsync({
            root: 'public/',
            hostname: config.server.host,
            port: config.server.port,
            username: config.server.username,
            destination: config.server.webroot,
            archive: true,
            silent: false,
            compress: true,
            progress: true
        }));
}


/**
* Tasks
* 
*/

exports.serve = series(parallel(styles, scripts, sprite), server, watchForChanges);
exports.watch = series(parallel(styles, scripts, sprite), watchForChanges);
exports.build = parallel(styles, scripts, sprite);


/**
* Other
*
* Various quick actions
* 
*/


export const zipapp = () => {
    return src(['public/**/*', '!public/app/{cache,cache/**}', '!public/{upload,upload/**}', '!public/{cutup,cutup/**}'])
        .pipe(zip('app.zip'))
        .pipe(dest('./shared'))
}
