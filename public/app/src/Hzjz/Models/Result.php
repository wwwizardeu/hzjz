<?php
namespace Hzjz\Models;

use Subframe\Model;

class Result extends Model {

	public const TABLE = 'results', KEY = 'id';

	public $id, $code, $result, $url, $comment, $views, $created_at;


	public static function getByCode($code) {
		$nalaz = self::fetch("SELECT * FROM ".self::TABLE." WHERE code = ?", [$code]);

		return $nalaz;
	}

	public static function getIdByCode($code) {
		$id = self::result("SELECT id FROM ".self::TABLE." WHERE code = ?", [$code]);

		return $id;
	}

	public static function existsByCode($code) {
		$exists = self::result("SELECT 1 FROM ".self::TABLE." WHERE code = ?", [$code]);

		return $exists;
	}

	public static function view($id) {
		self::query("UPDATE ".self::TABLE." SET views = views + 1 WHERE id = ?", [$id]);
	}

}
