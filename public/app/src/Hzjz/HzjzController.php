<?php
namespace Hzjz;

use Subframe\Controller;

class HzjzController extends Controller {

	protected function findUser($password) {
		foreach (ADMINS as $u => $p)
			if ($p == $password)
				return $u;

		return null;
	}

	protected function findLoggedInUser() {
		$cookie = $_COOKIE['user'] ?? null;
		foreach (ADMINS as $u => $p)
			if ($this->encodeCookie($u) == $cookie)
				$user_id = $u;

		return $user_id ?? null;
	}

	protected function encodeCookie($user_id) {
		$browser_id = preg_replace('~\d+~', '', $_SERVER['HTTP_USER_AGENT']);

		return sha1($user_id . $browser_id);
	}

}
