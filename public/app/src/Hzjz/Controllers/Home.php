<?php
namespace Hzjz\Controllers;

use Hzjz\HzjzController;
use Hzjz\Models\Result;

class Home extends HzjzController {

	function getIndex() {
		$this->view('index');
	}

	function postIndex() {
		if (!strlen($code = $_POST['code'] ?? ''))
			$this->throw('Nedostaje šifra nalaza.', 400);

		$result = Result::getByCode($code)
			or $this->throw("Nepostojeća šifra.", 400);
		if ($result->result != 'unresolved')
			Result::view($result->id);

		$this->json($result);
	}

	function postLogin() {
		$post = array_map('trim', $_POST);
		if (!strlen($password = $post['password'] ?? ''))
			throw new \Exception('Molimo, navedi zaporku.', 400);
		$remember = true;

		if (!($user_id = $this->findUser($password)))
			throw new \Exception('Neispravna zaporka.', 400);
		setcookie('user', $this->encodeCookie($user_id), $remember ? time()+3*30*24*3600 : 0, HOME);

		$this->json();
	}

	function getLogout() {
		setcookie('user', '', -1, HOME);

		$this->redirect(HOME);
	}

}
