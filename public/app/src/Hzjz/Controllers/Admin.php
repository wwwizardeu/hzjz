<?php
namespace Hzjz\Controllers;

use Hzjz\HzjzController;
use Hzjz\Models\Result;

class Admin extends HzjzController {

	public function __construct() {
		$user = $this->findLoggedInUser()
			or $this->throw('Molimo, prijavite se zaporkom.', 401);
	}

	public function getIndex() {
		$results = Result::getAll(-99999);

		$this->view('admin', get_defined_vars());
	}

	public function getUnos() {
		$result = new Result;

		$this->view('admin-entry', get_defined_vars());
	}

	public function postUnos() {
		$this->postNalaz(null);
	}

	public function getNalaz($code) {
		$result = Result::getByCode($code)
			or $this->throw("Nema nalaza sa šifrom \"$code\".", 404);

		$this->view('admin-entry', get_defined_vars());
	}

	public function postNalaz($code) {
		$post = array_map('trim', $_POST);
		if (!strlen($post['result']))
			throw new \Exception('Rezultat testa nije naveden.', 400);
		if (!strlen($post['code']))
			throw new \Exception('Šifra nije navedena.', 400);
		if (!preg_match('~^\w+$~', $post['code']))
			throw new \Exception('Šifra ne može imati neuobičajene znakove.', 400);

		if ($code)
			$id = Result::getIdByCode($code)
				or $this->throw("Nema nalaza sa šifrom \"$code\".", 404);

		$result = new Result(['id' => null] + $post);
		if ($result->code != $code && Result::existsByCode($result->code))
			$this->throw('Dana šifra se već koristi.', 400);

		if (($file = $_FILES['pdf'] ?? null)
				&& $file['error'] != UPLOAD_ERR_NO_FILE) {
			if ($file['type'] != 'application/pdf')
				throw new \Exception('Datoteka treba biti u PDF-obliku.', 500);
			if ($file['error'])
				throw new \Exception('Datoteka nije primljena. Molimo, probaj ponovo.', 500);

			$result->url = UPLOAD."/$result->code.pdf";
			if (!move_uploaded_file($file['tmp_name'], ROOT.$result->url))
				throw new \Exception('Datoteka se ne može pohraniti. Molimo, probaj kasnije.', 500);
		}

		if (isset($id))
			$result->update($id);
		else
			$id = $result->insert();

		$this->json(['id' => $id, 'code' => $result->code]);
	}

	public function deleteNalaz($code) {
		if (($id = Result::getIdByCode($code)))
			Result::delete($id);

		$this->json();
	}

	private function generateCode() {
		do {
			$code = strtoupper(base_convert(random_int(36**2, 36**3-1), 10, 36));
		} while (Result::existsByCode($code));

		return $code;
	}

}
