<?php include_once('header.php') ?>

<section class="o-wrap u-pt-4 u-pb-8">
    <h1 class="u-mb-half">Unos nalaza</h1>
    <p>
        <a href="<?=HOME?>admin" class="c-link c-link--primary">
            <?php sprite('arrow-circle-left', 'u-fill-current') ?>
            Povratak
        </a>
    </p>

    <form class="c-form c-form--entry js-entry-form" method="post">
        <div class="c-form__notice js-notice" hidden>
            <p><?php sprite('exclamation', 'u-fill-current u-mr-half u-h4') ?> <span></span></p>
        </div>

        <div class="c-form__item c-form__item--narrow">
            <label for="code">Šifra nalaza</label>
            <input type="text" name="code" id="code" value="<?=htmlspecialchars($result->code)?>" required>
        </div>

        <div class="c-form__item">
            <div class="c-form__label">Rezultat</div>

            <div class="c-form__block c-form__block--unresolved u-mb-1">
                <input type="radio" name="result" value="unresolved" id="unresolved" <?=$result->result == 'unresolved' ? 'checked':''?> required>
                <label for="unresolved">Nedovršen</label>
            </div>

            <div class="c-form__block c-form__block--negative">
                <input type="radio" name="result" value="negative" id="negative" <?=$result->result == 'negative' ? 'checked':''?> required>
                <label for="negative">Negativan</label>
            </div>
            <div class="c-form__block c-form__block--positive">
                <input type="radio" name="result" value="positive" id="positive" <?=$result->result == 'positive' ? 'checked':''?> required>
                <label for="positive">Pozitivan</label>
            </div>
            <div class="c-form__block c-form__block--undefined">
                <input type="radio" name="result" value="undefined" id="undefined" <?=$result->result == 'undefined' ? 'checked':''?> required>
                <label for="undefined">Neodređen</label>
            </div>
        </div>

        <div class="c-form__item">
            <label for="pdf">Datoteka [PDF]</label>
            <?php if ($result->url) { ?>
                <p><a href="<?=$result->url?>"><?=basename($result->url)?></a></p>
            <?php } ?>
            <input type="file" name="pdf" id="pdf">
        </div>

        <div class="c-form__item">
            <label for="comment">Napomena</label>
            <p>Posebna napomena ispitaniku</p>
            <textarea name="comment" id="comment" cols="30" rows="4"><?=htmlspecialchars($result->comment)?></textarea>
        </div>

        <div class="c-form__item">
            <?php if ($result->id) { ?>
                <button class="c-button c-button--primary" type="submit" name="button" value="leave">
                    Spremi izmjene
                </button>
            <?php } else { ?>
                <button class="c-button c-button--primary" type="submit" name="button" value="repeat">
                    Spremi i novi unos
                </button>
                <button class="c-button c-button--primary" type="submit" name="button" value="leave">
                    Spremi
                </button>
            <?php } ?>
        </div>
    </form>

</section>

<?php include_once('footer.php') ?>
