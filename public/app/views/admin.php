<?php include_once('header.php') ?>

<section class="o-wrap u-pt-4 u-pb-8">
    <h1 class="u-mb-half">Administracija</h1>
    <p>
        <a href="<?=HOME?>admin/unos" class="c-link c-link--primary">
            Unos nalaza
            <?php sprite('arrow-circle-right', 'u-fill-current') ?>
        </a>

        <a href="#" class="c-link c-link--primary u-ml-1 js-logout">
            Odjava
            <?php sprite('arrow-circle-right', 'u-fill-current') ?>
        </a>
    </p>


    <div class="u-mt-4">
        <table class="c-entries u-my-1 js-entries">
            <thead>
                <tr>
                    <th class="c-entry__id">Šifra</th>
                    <th class="c-entry__result">Rezultat</th>
                    <th class="c-entry__view">Pregledan</th>
                    <th class="c-entry__cta">Akcije</th>
                </tr>
            </thead>
            <tbody>

                <?php foreach ($results as $result) { ?>

                    <tr class="is-<?=$result->result?>" data-code="<?=$result->code?>">
                        <td class="c-entry__id"><?=$result->code?></td>
                        <td class="c-entry__result"><?=['positive' => 'POZITIVAN', 'negative' => 'NEGATIVAN', 'undefined' => 'NEODREĐEN', 'unresolved' => 'NEDOVRŠEN'][$result->result]?></td>
                        <td class="c-entry__view c-entry__view--<?=$result->views ? 'yes':'no'?>"><?=$result->views ? 'DA':'NE'?></td>
                        <td class="c-entry__cta">
                            <a href="<?=HOME?>admin/nalaz/<?=rawurldecode($result->code)?>" class="u-pa-half"><?php echo sprite('file', 'u-fill-current')?></a>
                            <a class="u-pa-half js-delete-entry"><?php echo sprite('trash', 'u-fill-current')?></a>
                        </td>
                    </tr>

                <?php } ?>

            </tbody>
        </table>
    </div>

</section>

<?php include_once('footer.php') ?>
