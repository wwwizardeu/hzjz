<?php include_once('header.php') ?>

<section class="c-front u-text-center">

    <div class="c-front__header">
        <h1 class="c-front__title u-mb-half">Dobrodošli</h1>
        <p class="c-front__subtitle">Nacionalno istraživanje znanja, stavova, ponašanja i prevalencije klamidijske infekcije među mladima</p>
    </div>

    <div class="c-front__form">
        <form class="c-form c-form--login js-login-form" method="post">
            <div class="c-form__item c-form__item--narrow">
                <label for="password">Unesi zaporku</label>
                <input type="password" name="password" id="password" required>
                <div class="c-form__error" hidden>
                    <?php sprite('exclamation', 'u-fill-current u-mr-half u-h4') ?> <span class="js-error-message"></span>
                </div>
            </div>
            <div class="c-form__item">
                <button type="submit" class="c-button c-button--primary">Prijavi se</button>
            </div>
        </form>
    </div>

</section>

<?php include_once('footer.php') ?>
