
</main>

<footer class="c-footer">
    <div class="c-footer__hashtags">
        <span>#zaštiti se</span>
        <span>#čuvajspolnozdravlje</span>
    </div>
    <div class="u-mb-1">
        Istraživanje <a href="https://serzam2020.ffzg.unizg.hr/" class="c-link"><strong>SERZAM2020</strong></a>
    </div>
    <div class="c-footer__logos">
    
        <a href="https://web2020.ffzg.unizg.hr/" target="_blank" class="c-footer__logo c-footer__logo--ffzg">
            <img src="<?=$ASSETS?>/images/logo-ffzg.png" alt="FFZG">
        </a>

        <a href="https://www.hzjz.hr" target="_blank" class="c-footer__logo c-footer__logo--hzjz">
            <img src="<?=$ASSETS?>/images/logo-hzjz.png" alt="HZJZ">
        </a>

        <a href="http://www.snz.unizg.hr/" target="_blank" class="c-footer__logo c-footer__logo--snz">
            <img src="<?=$ASSETS?>/images/logo-snz.jpeg" alt="SNZ">
        </a>

        <a href="https://stampar.hr/hr" target="_blank" class="c-footer__logo c-footer__logo--stampar">
            <img src="<?=$ASSETS?>/images/logo-stampar.png" alt="ŠTAMPAR">
        </a>
        
        <a href="https://hrzz.hr/" target="_blank" class="c-footer__logo c-footer__logo--hrzz">
            <img src="<?=$ASSETS?>/images/logo-hrzz.png" alt="HRZZ">
        </a>
    </div>
</footer>

<script src="<?=$ASSETS?>/vendor/jquery.min.js"></script>
<script src="<?=$ASSETS?>/js/app.js"></script>
<script src="<?=$ASSETS?>/vendor/datatables.min.js"></script>

</body>
</html>
