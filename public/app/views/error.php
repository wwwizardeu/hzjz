<?php include_once('header.php') ?>

<section class="c-front u-text-center">

    <div class="c-front__header">
        <h1 class="c-front__title u-mb-half">Neočekivana greška</h1>
        <p class="c-front__subtitle"><?=htmlspecialchars($error)?></p>
    </div>

</section>

<?php include_once('footer.php') ?>
