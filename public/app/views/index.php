<?php include_once('header.php') ?>

<section class="c-front u-text-center">

    <div class="c-front__header">
        <h1 class="c-front__title u-mb-half">Dobrodošli</h1>
        <p class="c-front__subtitle"><strong>Aplikacija za provjeru rezultata testiranja</strong></p>
        <p class="c-front__subtitle">Nacionalno istraživanje znanja, stavova, ponašanja i prevalencije klamidijske infekcije među mladima</p>
    </div>

    <div class="c-front__body c-front__body--unresolved" hidden>
        <p class="u-h2 u-text-center">
            Vaš uzorak još nije testiran.
        </p>

        <p class="u-text-center">Rezultat će biti dostupan u aplikaciji najranije 14 dana od slanja uzorka urina.</p>

        <p class="u-text-center">
            Ako imate dodatna pitanja možete kontaktirati istraživački tim na: <a href="mailto:serzam@ffzg.hr">serzam@ffzg.hr</a>.
        </p>

        <p class="u-text-center u-text-small" style="font-style: italic">
        <strong>Napomena:</strong> U iznimnim situacijama, moguće je da rezultat testiranja bude gotov nakon nešto više od 14 dana zbog organizacije dostave uzoraka i procedura u laboratoriju. Zahvaljujemo na razumijevanju.
        </p>
        
    </div>


    <div class="c-front__body c-front__body--positive" hidden>
        <p class="u-h2 u-text-center">
            Testiranje molekularnim PCR testom na klamidijsku infekciju je pokazalo  <strong class="u-text-primary u-font-bold">pozitivan rezultat</strong>.
        </p>
        <p>
            Pozitivan rezultat testiranja PCR testom znači da je ovim testom nađeno prisustvo bakterije klamidije u urinu. To znači da imate klamidijsku infekciju. Vaš nalaz je u privitku (možete ga isprintati). 
        </p>
        <p>
            Klamidijska infekcija uspješno se liječi antibioticima, ali je važno liječenje započeti što ranije i liječiti sve partnere istovremeno. Dok se ne provede antibiotsko liječenje, partnerima se savjetuje da se suzdrže od spolnih odnosa ili da koriste kondom pri svakom spolnom odnosu. Za daljnje upute i liječenje molimo Vas, javite se svom izabranom liječniku obiteljske medicine,  ginekologu ili dermatovenerologu radi daljnjih uputa i liječenja.
        </p>
        <p><strong>
            U sklopu istraživanja SERZAM možete kontaktirati i liječnika u Nastavnom zavodu za javno zdravstvo „Dr. Andrija Štampar“ za objašnjenje rezultata te savjetovanje i upute o liječenju. Liječnika možete kontaktirati na broj telefona: 01 / 46 96 121 ili 01 /46 96 172 radnim danom u vremenu od 10-13h do 15. siječnja 2021. godine.
        </strong></p>
        <p class="u-text-primary js-comment">
            Napomena liječnika: "<span></span>"
        </p>
        <p class="js-document">
            Vaš nalaz: <a class="u-ml-half"><?php echo sprite('file', 'u-fill-current')?> Preuzmi nalaz</a>
        </p>
    </div>

    <div class="c-front__body c-front__body--negative" hidden>
        <p class="u-h2 u-text-center">
            Testiranje molekularnim PCR testom na klamidijsku infekciju pokazalo je <strong style="color: green">negativan rezultat</strong>.
        </p>
        <p>
            Ako imate simptome koji bi mogli ukazivati na spolno prenosivu infekciju, kao što je peckanje pri mokrenju, promijenjeni ili pojačani iscjedak, bol u donjem dijelu trbuha, ili ako ste bili u riziku za infekciju (npr. imali ste nezaštićeni spolni odnos), javite se ginekologu, dermatovenerologu ili Vašem liječniku obiteljske medicine radi pregleda i savjeta za daljnje korake.
        </p>
        <p class="u-text-primary js-comment">
            Napomena liječnika: "<span></span>"
        </p>
        <p class="js-document">
            Vaš nalaz: <a class="u-ml-half"><?php echo sprite('file', 'u-fill-current')?> Preuzmi nalaz</a>
        </p>
    </div>


    <div class="c-front__body c-front__body--undefined" hidden>
        <p class="u-h2 u-text-center u-mb-half">
            Vaš uzorak se nažalost <strong class="u-text-primary">nije mogao testirati</strong>.
        </p>
        <p class="u-text-center">
            Razlog je neodgovarajući uzorak urina, a razlozi za nemogućnosti testiranja uzorka mogu biti: premala količina urina u epruveti, urin nije dobro stavljen u transportnu podlogu i slično. zaprimili.
        </p>
        <p class="u-mt-1">
            Ako imate simptome koji bi mogli ukazivati na spolno prenosivu infekciju, kao što je peckanje pri mokrenju, promijenjeni ili pojačani iscjedak, bol u donjem dijelu trbuha, ako ste bili u riziku za infekciju (npr. imali ste nezaštićeni spolni odnos), ili ako to nije slučaj, a željeli biste se savjetovati s liječnikom, javite se ginekologu, dermatovenerologu ili Vašem liječniku obiteljske medicine radi pregleda i odluke o potrebi testiranja na klamidiju.
        </p>
        <p class="u-text-primary js-comment">
            Napomena liječnika: "<span></span>"
        </p>
    </div>


    <div class="c-front__body c-front__body--static" hidden>
        <p><hr></p>

        <p class="u-text-small">
            Više informacija o projektu u sklopu kojeg se provodi ovo testiranje možete potražiti na internetskoj stranici: <a href="https://serzam2020.ffzg.unizg.hr/" target="_blank">serzam2020.ffzg.unizg.hr</a>
        </p>

        <p class="u-text-small">
            Ako imate pitanja o prevenciji i testiranju na klamidijsku infekciju i druge spolno prenosive bolesti ili o očuvanju spolnog zdravlja, možete ih postaviti u obrazac <strong>Pitaj stručnjaka</strong> koristeći internetsku platformu <a href="https://spolnozdravlje.hr/pitajstrucnjaka/1" target="_blank">Spolno zdravlje. </a>
        </p>

        <h4 class="u-mt-2">Savjeti za zaštitu od spolno prenosivih infekcija</h4>

        <ul class="u-ml-1">
            <li>Izbjegavajte rizična spolna ponašanja, poput čestog mijenjanja partnera.</li>
            <li>Koristite kondom pri svakom spolnom odnosu.</li>
        </ul>

        <p>Više informacija o zaštiti spolnog zdravlja možete potražiti na internetskoj platformi Spolno zdravlje: <a href="https://spolnozdravlje.hr" target="_blank">spolnozdravlje.hr</a> </p>

    </div>

    <div class="c-front__form">
        <form class="c-form c-form--login js-code-form" method="post">
            <div class="c-form__item c-form__item--narrow">
                <label for="code">Unesi šifru</label>
                <input type="text" name="code" id="code" required autofocus>

                <div class="c-form__error" hidden>
                    <?php sprite('exclamation', 'u-fill-current u-mr-half u-h4') ?> <span class="js-error-message"></span>
                </div>
            </div>
            <div class="c-form__item">
                <button type="submit" class="c-button c-button--primary">Provjeri</button>
            </div>
        </form>
    </div>

</section>

<?php include_once('footer.php') ?>
