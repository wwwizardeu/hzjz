<?php
use Subframe\Controller;
use Subframe\Model;

require './config.php';

// Automatske konstante
define('ROOT', __DIR__);
define('HOME', rtrim(dirname($_SERVER['SCRIPT_NAME']), '/').'/');

// Učitavanje templatesa
set_include_path(ROOT.'/app/views');

// Autoload-mehanizam
spl_autoload_register(function ($classname) {
	$path = strtr($classname, ['_' => '/', '\\' => '/']).'.php';
	@include ROOT."/app/src/$path";
});

header('Cache-Control: max-age=0,no-store,must-revalidate');
header('Vary: Accept-Encoding');
header('X-Frame-Options: sameorigin');
header('X-XSS-Protection: 1; mode=block');
header('X-Content-Type-Options: nosniff');
header('Strict-Transport-Security: max-age=31536000; includeSubDomains; preload');


new class() extends Controller {

	public function __construct() {
		try {
			// Connecting to the database
			Model::connect('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASS);

			// Finding a route
			self::routeInNamespace('Hzjz\Controllers', [], @$_SERVER['PATH_INFO']);

			// No route was found
			throw new Exception("Nažalost, na ovoj adresi nema sadržaja.", 404);
		}

		// Handling any errors
		catch (Throwable $e) {
			// Finding error code and message
			$code = ($e->getCode() >= 400 && $e->getCode() <= 500 ? $e->getCode() : 500);
			$data = ['error' => $e->getMessage()];
			if ($code != 401 && $code != 404)
				error_log($e->getMessage().' '.$e->getTraceAsString());

			// Outputting error message in the appropriate form
			if (self::isAjax())
				$this->json($data, $code);
			elseif ($code == 401)
				$this->view('admin-login', $data, $code);
			else
				$this->view('error', $data, $code);
		}
	}

};
