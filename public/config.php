<?php
// Pristupni podaci za bazu podataka
define('DB_NAME', $_SERVER['DB_NAME'] ?? '');
define('DB_USER', $_SERVER['DB_USER'] ?? '');
define('DB_PASS', $_SERVER['DB_PASS'] ?? '');
define('DB_HOST', $_SERVER['DB_HOST'] ?? 'localhost');

// Administrator(i)
define('ADMINS', [
	'admin' => 'test',
]);

// Lokacija za upload PDF-ova
define('UPLOAD', '/upload');
