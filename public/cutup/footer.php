
</main>

<footer class="c-footer">
    <a href="https://www.hzjz.hr" target="_blank">
        <img src="<?=$ASSETS?>/images/logo-hzjz.png" alt="HZJZ">
    </a>
</footer>

<script type="text/javascript" src="<?=$ASSETS?>/vendor/jquery.min.js"></script>
<script src="<?=$ASSETS?>/js/app.js"></script>    
<script type="text/javascript" src="<?=$ASSETS?>/vendor/datatables.min.js"></script>

</body>
</html>
