<?php

// CUTUP HELPERS
$HOST   = (@$_SERVER['HTTPS'] ? 'https://':'http://') . $_SERVER[ 'SERVER_NAME' ];
$ASSETS = (@$_SERVER['HTTPS'] ? 'https://':'http://') . $_SERVER[ 'SERVER_NAME' ] . '/assets';

$url = parse_url($_SERVER['REQUEST_URI']);
$url_parts = explode('/', $url['path']);
$SECTION = $url_parts[2];

// FUNCTIONS
/**
 * Sprite function.
 *
 * Example of usage:
 * ```php
 * <?php sprite('arrow-left', 'u-fill-current'); ?>
 * ```
 *
 * @param string  $name    SVG icon name.
 * @param string  $classes Additional classes.
 * @param boolean $echo    Echo or return.
 * @return void|string
 */

function sprite( $name, $classes = '', $echo = true ) {
    global $ASSETS;
    $path = $ASSETS . '/sprite/sprite.svg#' . $name;
    $output = "<svg class=\"o-icon {$classes}\"><use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"{$path}\"></use></svg>";

    if ( ! $echo ) {
        return $output;
    }

    echo $output; // WPCS: xss ok.
}





?>
