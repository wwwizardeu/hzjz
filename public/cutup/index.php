<?php include_once('header.php') ?>

<section class="c-front u-text-center">

    <div class="c-front__header">
        <h1 class="c-front__title u-mb-half">Dobrodošli</h1>
        <p class="c-front__subtitle">Nacionalno istraživanje znanja, stavova, ponašanja i prevalencije klamidijske infekcije među mladima</p>
    </div>

    <div class="c-front__body c-front__body--unresolved" style="display: none">
        <p class="u-h2 u-text-center">
            Rezultat Vašeg testa <strong class="u-text-primary">još nije gotov/unesen</strong>. 
        </p>
        <p class="u-text-center">
            Molimo Vas, pogledajte rezultat u aplikaciji najranije 14 dana od slanja uzorka.
        </p>
        <p class="u-text-center">
        Napomena: Može se dogoditi da rezultat testiranja bude gotov za nešto više od 14 dana zbog organizacije dostave uzoraka i procedura u laboratoriju, pa Vas molimo za razumijevanje.
        </p>
    </div>


    <div class="c-front__body c-front__body--positive" style="display: none">
        <p class="u-h2 u-text-center">
            Testiranje molekularnim PCR testom na klamidijsku infekciju je pokazalo <strong class="u-text-primary u-font-bold">pozitivan rezultat</strong>.
        </p>
        <p>
            Pozitivan rezultat testiranja PCR testom znači da je ovim testom nađeno prisustvo bakterije klamidije u urinu. To znači da imate klamidijsku infekciju. Molimo Vas javite se svom liječniku obiteljske medicine ili ginekologu (djevojke) radi daljnjih uputa i liječenja. 
        </p>
        <p>
        Klamidijska infekcija uspješno se liječi antibioticima, ali je važno liječenje započeti što ranije i liječiti oba partnera.
        </p>
        <p class="u-text-primary">
            Napomena liječnika: "lorem ipsum"
        </p>
        <p>
            Vaš nalaz: <a href="#pdf">Preuzmi nalaz</a>
        </p>
    </div>

    <div class="c-front__body c-front__body--negative" style="display: none">
        <p class="u-h2 u-text-center">
            Testiranje molekularnim PCR testom na klamidijsku infekciju je pokazalo <strong class="u-text-primary">negativan rezultat</strong>.
        </p>
        <p>
            Negativan rezultat testiranja PCR testom znači da ovim testom nije nađeno prisustvo bakterije klamidije u urinu.   
        </p>
        <p>
            No, ako imate simptome koji bi mogli ukazivati na spolno prenosivu infekciju, kao što je peckanje pri mokrenju, promijenjeni ili pojačani iscjedak, bol u donjem dijelu trbuha, ili ste bili u riziku za infekciju (nezaštićeni spolni odnos), javite se liječniku radi pregleda i savjeta za daljnje korake.
        </p>
        <p class="u-text-primary">
            Napomena liječnika: "lorem ipsum"
        </p>
        <p>
            Vaš nalaz: <a href="#pdf">Preuzmi nalaz</a>
        </p>
    </div>


    <div class="c-front__body c-front__body--undefined" style="display: none">
        <p class="u-h2 u-text-center u-mb-half">
            Vaš uzorak se nažalost <strong class="u-text-primary">nije mogao testirati</strong> zbog neodgovarajućeg uzorka koji smo zaprimili.
        </p>
        <p class="u-text-center u-text-small">
            (npr. premala količina urina u epruveti, urin nije bio u transportnoj podlozi i sl.)
        </p>
        <p class="u-mt-1">
            Savjetujte se s Vašim liječnikom o potrebi ponavljanja testiranja.   
        </p>
        <p class="u-text-primary">
            Napomena liječnika: "lorem ipsum"
        </p>
    </div>


    <div class="c-front__body c-front__body--static" style="display: none">
        <p><hr></p>
        <p class="u-text-small">
            Ako imate dodatna pitanja možete nazvati na telefon XXX / XXXX XX ili e-mail: <a href="mailto:serzam@ffzg.hr">serzam@ffzg.hr</a>
        </p>

        <h4 class="u-mt-2">Savjeti za zaštitu od spolno prenosivih infekcija</h4>

        <ul class="u-ml-1">
            <li>Izbjegavati rizično spolno ponašanje.</li>
            <li>Izbjegavati često mijenjanje partnera.</li>
            <li>Upotrebljavati kondom pri spolnom odnosu.</li>
            <li>Izbjegavati spolne odnose pod utjecajem alkohola ili psihoaktivnih sredstava (droge).</li>
        </ul>

        <p>Više informacija o zaštiti spolnog zdravlja možete potražiti na internetskoj platformi Spolno zdravlje: <a href="https://spolnozdravlje.hr" target="_blank">spolnozdravlje.hr</a> </p>

    </div>

    <div class="c-front__form" style="display: -none">
        <form action="" class="c-form c-form--login">
            <div class="c-form__item c-form__item--narrow">
                <label for="code">Unesi šifru</label>
                <input type="text" name="code" id="code">
            </div>
            <div class="c-form__item">
                <button type="submit" class="c-button c-button--primary">Provjeri</button>
            </div>
        </form>
    </div>
    
</section>

<?php include_once('footer.php') ?>
