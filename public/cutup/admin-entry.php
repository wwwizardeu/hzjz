<?php include_once('header.php') ?>

<section class="o-wrap u-pt-4 u-pb-8">
    <h1 class="u-mb-half">Unos rezultata</h1>
    <p>
        <a href="admin.php" class="c-link c-link--primary">
            <?php sprite('arrow-circle-left', 'u-fill-current') ?>
            Povratak
        </a>
    </p>

    <form action="" class="c-form c-form--entry">
        <div class="c-form__notice">
            <p><?php sprite('exclamation', 'u-fill-current u-mr-half u-h4') ?> Rezultat je uspješno unesen!</p>
            <!-- <p>Šifra već postoji u sustavu!</p> -->
        </div>
        <div class="c-form__item c-form__item--narrow">
            <label for="code">Šifra nalaza</label>
            <input type="text" name="code" id="code" required>
        </div>
        <div class="c-form__item">
            <div class="c-form__label">Rezultat</div>

            <div class="c-form__block c-form__block--unresolved u-mb-1">
                <input type="radio" name="result" value="unresolved" id="unresolved" checked required>
                <label for="unresolved">Nedovršen</label>
            </div>

            <div class="c-form__block c-form__block--negative">
                <input type="radio" name="result" value="false" id="false" required>
                <label for="false">Negativan</label>
            </div>
            <div class="c-form__block c-form__block--positive">
                <input type="radio" name="result" value="true" id="true" required>
                <label for="true">Pozitivan</label>
            </div>
            <div class="c-form__block c-form__block--undefined">
                <input type="radio" name="result" value="undefined" id="undefined" required>
                <label for="undefined">Neodređen</label>
            </div>
        </div>
        <div class="c-form__item">
            <label for="pdf">Rezultat [PDF]</label>
            <input type="file" name="pdf" id="pdf">
        </div>
        <div class="c-form__item">
            <label for="comment">Napomena</label>
            <p>Posebna napomena ispitaniku</p>
            <textarea name="comment" id="comment" cols="30" rows="4"></textarea>
        </div>
        <div class="c-form__item">
            <button class="c-button c-button--primary" type="submit">
                Spremi i novi unos
            </button>
            <button class="c-button c-button--primary" type="submit">
                Spremi
            </button>
        </div>
    </form>
    
    

</section>

<?php include_once('footer.php') ?>
