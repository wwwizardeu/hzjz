<?php include_once('header.php') ?>

<section class="o-wrap u-pt-4 u-pb-8">
    <h1 class="u-mb-half">Administracija</h1>
    <p>
        <a href="admin-entry.php" class="c-link c-link--primary">
            Unos rezultata
            <?php sprite('arrow-circle-right', 'u-fill-current') ?>
        </a>
    </p>

    
    <div class="u-mt-4">
        <table class="c-entries u-my-1 js-entries">
            <thead>
                <tr>
                    <th class="c-entry__id">Šifra</th>
                    <th class="c-entry__result">Rezultat</th>
                    <th class="c-entry__view">Pregledan</th>
                    <th class="c-entry__cta">Akcije</th>
                </tr>
            </thead>
            <tbody>
                <tr class="is-positive">
                    <td class="c-entry__id">2311</td>
                    <td class="c-entry__result">POZITIVAN</td>
                    <td class="c-entry__view c-entry__view--yes">DA</td>
                    <td class="c-entry__cta">
                        <a href="admin-entry.php" class="u-pa-half"><?php echo sprite('file', 'u-fill-current')?></a>
                        <a href="#delete-entry" class="u-pa-half"><?php echo sprite('trash', 'u-fill-current')?></a>
                    </td>
                </tr>
                <tr class="is-negative">
                    <td class="c-entry__id">4879</td>
                    <td class="c-entry__result">NEGATIVAN</td>
                    <td class="c-entry__view c-entry__view--no">NE</td>
                    <td class="c-entry__cta">
                        <a href="admin-entry.php" class="u-pa-half"><?php echo sprite('file', 'u-fill-current')?></a>
                        <a href="#delete-entry" class="u-pa-half"><?php echo sprite('trash', 'u-fill-current')?></a>
                    </td>
                </tr>
                <tr class="is-undefined">
                    <td class="c-entry__id">3987</td>
                    <td class="c-entry__result">NEODREĐEN</td>
                    <td class="c-entry__view c-entry__view--yes">DA</td>
                    <td class="c-entry__cta">
                        <a href="admin-entry.php" class="u-pa-half"><?php echo sprite('file', 'u-fill-current')?></a>
                        <a href="#delete-entry" class="u-pa-half"><?php echo sprite('trash', 'u-fill-current')?></a>
                    </td>
                </tr>
                <tr class="is-unresolved">
                    <td class="c-entry__id">2847</td>
                    <td class="c-entry__result">NEDOVRŠEN</td>
                    <td class="c-entry__view c-entry__view--yes">-</td>
                    <td class="c-entry__cta">
                        <a href="admin-entry.php" class="u-pa-half"><?php echo sprite('file', 'u-fill-current')?></a>
                        <a href="#delete-entry" class="u-pa-half"><?php echo sprite('trash', 'u-fill-current')?></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

</section>

<?php include_once('footer.php') ?>
