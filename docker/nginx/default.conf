server {
    location ~ /\.ht {
        deny all;
    }

    location ^~ /app/ {
        deny all;
        return 403;
    }

    listen 80 default_server;
    root /var/www/html;
    index index.html index.php;

    listen 443 ssl;
    server_name tov.test;
    ssl_certificate /etc/nginx/ssl/server.crt;
    ssl_certificate_key /etc/nginx/ssl/server.key;

    charset utf-8;

    # Use the prod site for missing images
    location ~* /uploads/(.*) {
        try_files /uploads/$1 @proxy;
    }

    # URL to the production site should be set
    location @proxy{
        proxy_pass https://tjedanodmoravrijedan.hr;
        proxy_set_header X-Real-IP  $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    access_log off;
    error_log  /var/log/nginx/error.log error;

    sendfile off;

    client_max_body_size 100m;

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_intercept_errors off;
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
    }
}